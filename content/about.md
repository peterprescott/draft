---
title: "About"
date: 2019-08-29T16:59:43+01:00
---

> *Do whatever is in your heart. I'm with you heart and soul.*
> 
> [1 Samuel 14:7](https://biblehub.com/1_samuel/14-7.htm)

DRAFT is here to help you (and also [future me](https://peter.prescott.org.uk/future-me)) set up a website so that you can start [communicating](https://www.christianstogether.net/Articles/89889/Christians_Together_in/Christian_Life/Is_there_any/Jean_Darnalls_Vision.aspx) whatever is in your heart. It's been adapted from [Calin Tat](https://calintat.github.io/project/)'s [Minimal theme](https://github.com/calintat/minimal/).

DRAFT has two simple values:

1. **Lean** -- like [Louise Parker](https://louiseparker.com/). And [Eric Ries](http://theleanstartup.com/).
2. **Tidy** -- like [Marie Kondo](https://konmari.com/). And [Hadley Wickham](http://hadley.nz/).

Now, lean-ness is [not just thin-ness](https://www.healthguidance.org/entry/3547/1/being-thin-and-being-lean-is-there-a-difference.html), and life-changing tidyness [isn't just minimalism](http://plaidfuzz.com/konmari/). 

So rather than being the very simplest [one-click to launch blog](https://www.blogger.com), DRAFT does require that you set yourself up like a real web developer. 

You will need to install (if they're not already on your computer) [git](https://git-scm.com) and [Hugo](https://gohugo.io/getting-started/installing/). If you're on Windows, it's probably easiest to first install [Choco](https://chocolatey.org/install), so that you can then just install the other programs automatically with one line of text. I think [Homebrew](https://brew.sh/) is similar for Mac. Don't worry, these are all free -- and open-source.

And you will need to sign up for accounts with [GitLab](https://gitlab.com) and [Netlify](https://netlify.com). Again, these are free -- because they make their money from selling services to big tech companies rather than to little individuals like you and me.

But the few more minutes it takes you to set up will be worth it, because it will ultimately give you move control and more flexibility.  And it will be cheaper -- something like Squarespace or self-hosted Wordpress will cost you about £10/month = £120/year; while DRAFT is set up for you to be hosted for free on Netlify, so all you need to pay is £5/year (or so) for a domain name.

So -- once you've done the necessary -- go ahead and clone a copy of DRAFT:

````
git clone https://gitlab.com/peterprescott/draft
````

Then go ahead and adapt it however you like. Most probably you want to change the files in the **content** folder. Use git to commit your changes. Then create [a new GitLab project repository](https://gitlab.com/projects/new) (don't tick the box to initialize the project with a README file), and push your adapted site up to Gitlab.

````
git add *
git commit -m "make it my own"
git remote add origin https://gitlab.com/YOUR-USER-NAME/YOUR-PROJECT-NAME.git
git push -u origin master
````

Then tell Netlify to [create a new site from Git](https://app.netlify.com/start), with its *Build command* set to 'hugo --gc --minify' and its *Publish directory* set to 'public'. And then you're off!

Oh, and if you want to change your domain name, you can go to *Site Settings* > *Domain Management* and *Add Custom Domain*. (You can't buy .uk domains through Netlify -- I recommend [Ecenica](https://www.ecenica.com/dashboard/aff.php?aff=907).)
